#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "CalibratioActor.h"
#include "Cluster/IDisplayClusterClusterManager.h"

enum class CurrentViewMode : uint8
{
    GameAndUI,
    GameOnly,
    UIOnly,
    Invalid
};

class FCalibratioModule : public IModuleInterface
{
public:
    static bool IsRoomMountedMode();
    template <class T>
    static bool LoadClass(const FString& Path, TSubclassOf<T>& Result);
    template <class T>
    static bool LoadAsset(const FString& Path, T*& Result);
    static bool IsMaster();
    static CurrentViewMode GetCurrentViewMode(const APlayerController *PlayerController);
    virtual void StartupModule () override;
	virtual void ShutdownModule() override;

	static void SpawnCalibratio();

private:
	IConsoleCommand* CalibratioConsoleCommand = nullptr;
	FOnClusterEventJsonListener ClusterEventListenerDelegate;
};
