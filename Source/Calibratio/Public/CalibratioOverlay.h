#pragma once

#include "CoreMinimal.h"

#include "CalibratioActor.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"

#include "CalibratioOverlay.generated.h"


/**
 * This is the parent class for the Overlay that is used on the master.
 * All declarations in it are magically bound to the UMG child class if they are named the same (see "meta = (BindWidget)")
 */
UCLASS()
class CALIBRATIO_API UCalibratioOverlay : public UUserWidget
{
	GENERATED_BODY()

virtual bool Initialize() override;
public:
	/* Public Buttons */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* ResettingButton;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* DismissButton;

public:
	void SetOwner(ACalibratioActor* InOwner) {Owner = InOwner;}
	UFUNCTION() void Dismiss();

private:
	UPROPERTY() ACalibratioActor* Owner;
};
