#pragma once

#include "CoreMinimal.h"
#include "Calibratio.h"
#include "MotionControllerComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Cluster/IDisplayClusterClusterManager.h"

#include "GameFramework/Actor.h"
#include "CalibratioActor.generated.h"

UCLASS()
class CALIBRATIO_API ACalibratioActor : public AActor
{
	GENERATED_BODY()

public:
    ACalibratioActor();

protected:
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;
	virtual void Tick(float DeltaSeconds) override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	UPROPERTY(VisibleAnywhere) UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere) UMaterialInterface* BaseMaterial;
	UPROPERTY(VisibleAnywhere) UMotionControllerComponent* MotionSource;
	UFUNCTION(Blueprintcallable) void ClusterReset();
	UFUNCTION(Blueprintcallable) void ClusterDespawn();

private:
	// Handling Cluster events
	FOnClusterEventJsonListener ClusterEventListenerDelegate;
	void LocalDespawn();
	void LocalReset();
	void HandleClusterEvent(const FDisplayClusterClusterEventJson& Event);
	const FString EventCategory = "Calibratio";
	const FString EventDespawn = "CalibratioDespawn";
	const FString EventReset = "CalibratioReset";

	// Calibration
	FVector CalibratedPosition = FVector(0, 0, 0);
	bool needsReset = false;
	const FName MotionSourceName = FName("Unknown-DTrack-Body-09");

	// Overlay & UI stuff
	UPROPERTY() UMaterialInstanceDynamic* DynamicMaterial = nullptr;
	TSubclassOf<class UCalibratioOverlay> Overlay_Class;
	UPROPERTY() UCalibratioOverlay* Overlay;
	CurrentViewMode PreviousInputMode = CurrentViewMode::Invalid;
};
