#include "Calibratio.h"
#include "Kismet/GameplayStatics.h"
#include "IDisplayCluster.h"
#include "Cluster/DisplayClusterClusterEvent.h"

#define LOCTEXT_NAMESPACE "FCalibratioModule"

bool FCalibratioModule::IsRoomMountedMode()
{
#if PLATFORM_WINDOWS || PLATFORM_LINUX
	return IDisplayCluster::Get().GetOperationMode() == EDisplayClusterOperationMode::Cluster;
#else
	return false;
#endif
}

template <typename T>
bool FCalibratioModule::LoadClass(const FString& Path, TSubclassOf<T> & Result)
{
    ConstructorHelpers::FClassFinder<T> Loader(*Path);
	Result = Loader.Class;
	if (!Loader.Succeeded()) UE_LOG(LogTemp, Error, TEXT("Could not find %s. Have you renamed it?"), *Path);
	return Loader.Succeeded();
}

template <typename T>
bool FCalibratioModule::LoadAsset(const FString& Path, T* & Result)
{
	ConstructorHelpers::FObjectFinder<T> Loader(*Path);
	Result = Loader.Object;
	if (!Loader.Succeeded()) UE_LOG(LogTemp, Error, TEXT("Could not find %s. Have you renamed it?"), *Path);
	return Loader.Succeeded();
}

bool FCalibratioModule::IsMaster()
{
#if PLATFORM_WINDOWS || PLATFORM_LINUX
	if (!IDisplayCluster::IsAvailable()) 
	{
		return true;
	}
	IDisplayClusterClusterManager* Manager = IDisplayCluster::Get().GetClusterMgr();
	if (Manager == nullptr)
	{
		return true; // if we are not in cluster mode, we are always the master
	}
	return Manager->IsPrimary() || !Manager->IsSecondary();
#else
    return true;
#endif
}

CurrentViewMode FCalibratioModule::GetCurrentViewMode(const APlayerController *PlayerController)
{
	
    if (!IsValid(PlayerController)) return CurrentViewMode::Invalid;

    UGameViewportClient* GameViewportClient = PlayerController->GetWorld()->GetGameViewport();

    const bool bIgnoreInput = GameViewportClient->IgnoreInput();
    const EMouseCaptureMode CaptureMode = GameViewportClient->GetMouseCaptureMode();

    if (bIgnoreInput == false && CaptureMode == EMouseCaptureMode::CaptureDuringMouseDown)
    {
        return CurrentViewMode::GameAndUI;
    }

    if (bIgnoreInput == true && CaptureMode == EMouseCaptureMode::NoCapture)
    {
        return CurrentViewMode::UIOnly;
    }

    return CurrentViewMode::GameOnly;
}

void FCalibratioModule::StartupModule ()
{
	/* Registering console command */
	CalibratioConsoleCommand = IConsoleManager::Get().RegisterConsoleCommand(TEXT("Calibratio"), TEXT("Spawn an instance of calibratio"),
		FConsoleCommandWithArgsDelegate::CreateLambda([](const TArray< FString >&)
	{
		if(IsRoomMountedMode()){
			/* Emitting cluster event to spawn on all nodes in sync*/
			FDisplayClusterClusterEventJson ClusterEvent;
			ClusterEvent.Name = "CalibratioSpawn";
			ClusterEvent.Category = "CalibratioSpawner";
			IDisplayCluster::Get().GetClusterMgr()->EmitClusterEventJson(ClusterEvent, false);
		} else {
			SpawnCalibratio();
		}
	}));

	
	/* Register cluster event listening */
	IDisplayCluster* DisplayCluster = FModuleManager::LoadModulePtr<IDisplayCluster>(IDisplayCluster::ModuleName);
	if (DisplayCluster && !ClusterEventListenerDelegate.IsBound())
	{
		ClusterEventListenerDelegate = FOnClusterEventJsonListener::CreateLambda([](const FDisplayClusterClusterEventJson& Event)
		{
			if (Event.Category.Equals("CalibratioSpawner") && Event.Name.Equals("CalibratioSpawn"))
			{
				SpawnCalibratio();
			}
		});
		DisplayCluster->GetClusterMgr()->AddClusterEventJsonListener(ClusterEventListenerDelegate);
	}
}
void FCalibratioModule::ShutdownModule()
{
	IConsoleManager::Get().UnregisterConsoleObject(CalibratioConsoleCommand);
	IDisplayCluster::Get().GetClusterMgr()->RemoveClusterEventJsonListener(ClusterEventListenerDelegate);
}

void FCalibratioModule::SpawnCalibratio()
{
	if (UGameplayStatics::GetActorOfClass(GEngine->GetCurrentPlayWorld(), ACalibratioActor::StaticClass()) != nullptr) return;
	GEngine->GetCurrentPlayWorld()->SpawnActor<ACalibratioActor>();
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FCalibratioModule, Calibratio)