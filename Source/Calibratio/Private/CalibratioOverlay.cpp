#include "CalibratioOverlay.h"

bool UCalibratioOverlay::Initialize()
{
	const bool Result = Super::Initialize();

	if(DismissButton) DismissButton->OnClicked.AddDynamic(this, &UCalibratioOverlay::Dismiss);

    return Result;
}

void UCalibratioOverlay::Dismiss()
{
	Owner->ClusterDespawn();
}
