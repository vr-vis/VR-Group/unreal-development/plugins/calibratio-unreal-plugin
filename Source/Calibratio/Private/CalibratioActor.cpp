#include "CalibratioActor.h"

#include "Calibratio.h"
#include "Components/StaticMeshComponent.h"
#include "Cluster/DisplayClusterClusterEvent.h"
#include "IDisplayCluster.h"
#include "CalibratioOverlay.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Kismet/GameplayStatics.h"

ACalibratioActor::ACalibratioActor()
{
	PrimaryActorTick.bCanEverTick = true;
	AutoReceiveInput = EAutoReceiveInput::Player0;

	/* Loads needed Assets */
	UStaticMesh* CylinderMesh = nullptr;
	FCalibratioModule::LoadClass("WidgetBlueprint'/Calibratio/CalibratioHud'", Overlay_Class);
	FCalibratioModule::LoadAsset("StaticMesh'/Calibratio/Cylinder'", CylinderMesh);
	FCalibratioModule::LoadAsset("Material'/Calibratio/CalibratioMaterial'", BaseMaterial);

	/* LiveLink Data Source */
	MotionSource = CreateDefaultSubobject<UMotionControllerComponent>("MotionSource");
	MotionSource->SetTrackingMotionSource(MotionSourceName);
	SetRootComponent(MotionSource);

	/* Create Mesh component and initialize */
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetStaticMesh(CylinderMesh);
	Mesh->SetRelativeScale3D(FVector(1.0f,1.0f,0.1f)); //Make it a Disk
	Mesh->SetupAttachment(MotionSource);
}

// Called when the game starts or when spawned
void ACalibratioActor::BeginPlay()
{
	Super::BeginPlay();

	// Register cluster event listener
	IDisplayClusterClusterManager* ClusterManager = IDisplayCluster::Get().GetClusterMgr();
	if (ClusterManager && !ClusterEventListenerDelegate.IsBound())
	{
		ClusterEventListenerDelegate = FOnClusterEventJsonListener::CreateUObject(this, &ACalibratioActor::HandleClusterEvent);
		ClusterManager->AddClusterEventJsonListener(ClusterEventListenerDelegate);
	}

	// Create Overlay
	Overlay = CreateWidget<UCalibratioOverlay>(GetWorld()->GetFirstPlayerController(), Overlay_Class);
	Overlay->AddToViewport(0);
	Overlay->SetOwner(this);

	// Bind Buttons
	Overlay->ResettingButton->OnClicked.AddDynamic(this, &ACalibratioActor::ClusterReset);
	Overlay->DismissButton->OnClicked.AddDynamic(this, &ACalibratioActor::ClusterDespawn);

	// Hide this overlay on all clients
	if (!FCalibratioModule::IsMaster())
	{
		Overlay->SetVisibility(ESlateVisibility::Hidden);
	}

	// Attach to CAVE-Root
	APawn* Pawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	AttachToActor(Pawn, FAttachmentTransformRules::KeepRelativeTransform);
	SetOwner(Pawn);

	//Cache input mode
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	PreviousInputMode = FCalibratioModule::GetCurrentViewMode(PlayerController);
	PlayerController->SetInputMode(FInputModeUIOnly());
}

void ACalibratioActor::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	/* Create dynamic materials at runtime (Not constructor) */
	DynamicMaterial = UMaterialInstanceDynamic::Create(BaseMaterial, RootComponent);
	DynamicMaterial->SetVectorParameterValue("Color", FColor::Red);
	Mesh->SetMaterial(0, DynamicMaterial);
}

void ACalibratioActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	/* Remove from Cluster events */
	IDisplayClusterClusterManager* ClusterManager = IDisplayCluster::Get().GetClusterMgr();
	if (ClusterManager && ClusterEventListenerDelegate.IsBound())
	{
		ClusterManager->RemoveClusterEventJsonListener(ClusterEventListenerDelegate);
	}

	Super::EndPlay(EndPlayReason);

	/* Remove components */
	if(Overlay->IsInViewport()) Overlay->RemoveFromParent();
}

void ACalibratioActor::HandleClusterEvent(const FDisplayClusterClusterEventJson& Event)
{
	if(!Event.Category.Equals(EventCategory)) return; //Not our Business
	
	if (Event.Name == EventReset)
	{
		LocalReset();
	}
	else if (Event.Name == EventDespawn)
	{
		LocalDespawn();
	}
}

void ACalibratioActor::ClusterReset()
{
	if(!FCalibratioModule::IsRoomMountedMode()) return;
	
	IDisplayClusterClusterManager* const Manager = IDisplayCluster::Get().GetClusterMgr();
	if (!Manager) return;

	FDisplayClusterClusterEventJson ClusterEvent;
	ClusterEvent.Name = EventReset;
	ClusterEvent.Category = EventCategory;
	Manager->EmitClusterEventJson(ClusterEvent, true);
}

void ACalibratioActor::LocalReset()
{
	DynamicMaterial->SetVectorParameterValue("Color", FColor::Red);
	CalibratedPosition = FVector::ZeroVector;
	needsReset = true;
}

void ACalibratioActor::ClusterDespawn()
{
	if (!FCalibratioModule::IsRoomMountedMode()) return;
	
	IDisplayClusterClusterManager* const Manager = IDisplayCluster::Get().GetClusterMgr();
	if (!Manager) return;

	FDisplayClusterClusterEventJson ClusterEvent;
	ClusterEvent.Name = EventDespawn;
	ClusterEvent.Category = EventCategory;
	Manager->EmitClusterEventJson(ClusterEvent, true);
}
void ACalibratioActor::LocalDespawn()
{
	// Set input mode to previous one
	APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	switch(PreviousInputMode){
		case CurrentViewMode::GameAndUI:
			playerController->SetInputMode(FInputModeGameAndUI());
			break;
        case CurrentViewMode::UIOnly:
			playerController->SetInputMode(FInputModeUIOnly());
			break;
		case CurrentViewMode::GameOnly:
			playerController->SetInputMode(FInputModeGameOnly());
			break;
        case CurrentViewMode::Invalid:
        default: ;
    }

	GetWorld()->DestroyActor(this); // Destroy ourself
}

void ACalibratioActor::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if(needsReset)
	{
	    CalibratedPosition = MotionSource->GetRelativeLocation();
		needsReset = false;
		return;
	}

	//Distance bigger than threshold
	if(FVector::Distance(CalibratedPosition,MotionSource->GetRelativeLocation()) > 3)
	{
		DynamicMaterial->SetVectorParameterValue("Color", FColor::White);
	}
	else
	{
	    DynamicMaterial->SetVectorParameterValue("Color", FColor::Black);
	}
}